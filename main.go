package main

import (
	"log"
	"looper/browser"
	"looper/parser"
	"looper/player"
	"looper/usb"
	"os/exec"
	"time"
)

var splashFlag bool
var Label string = "LOOPER"
var Target string = "/home/adam/looper"

func usbRead(label string, target string, device usb.Device) []parser.Asset {
	var assets []parser.Asset
	devConnected := usb.CheckDeviceConnected(device)
	if devConnected == true {
		mountSuccess := usb.MountDevice(Label, Target)
		if mountSuccess == true {
			assets = parser.ReadFilesFromPath(Target)
			return assets
		} else {
			return assets
		}

	} else {
		return assets
	}
}

func initState(assetCounter int, assets []parser.Asset) string {

	if len(assets) == 0 {
		return "SPLASH"
	}
	return assets[assetCounter].AssetType
}

func browserState(assetCounter int, assets []parser.Asset, browserSocket string) string {
	splashFlag = false
	browser.LoadUrl(browserSocket, assets[assetCounter].Path)
	time.Sleep(time.Second * time.Duration(assets[assetCounter].Timeout))
	return "INIT"
}

func videoState(assetCounter int, assets []parser.Asset, mpvSocketPath string, playerState chan string) string {
	splashFlag = false
	cmd := exec.Command("/usr/bin/mpv", "-input-ipc-server="+mpvSocketPath, "-idle=yes")
	quit := make(chan bool)
	execErr := cmd.Start()
	time.Sleep(1 * time.Second)
	if execErr != nil {
		log.Println(execErr.Error())
	}
	go player.SocketWorker(mpvSocketPath, quit)
	player.LoadFile(mpvSocketPath, assets[assetCounter].Path)
	player.OnFullscreen(mpvSocketPath)
	go player.ReadSocket(mpvSocketPath, playerState)
	_ = player.EndListener(playerState)
	quit <- true
	cmd.Process.Kill()
	return "INIT"
}

func splashState(assetCounter int, assets []parser.Asset, browserSocket string, splashPath string) string {
	if splashFlag == false {
		browser.LoadUrl(browserSocket, splashPath)
		splashFlag = true

	}
	return "INIT"
}

func main() {

	playerState := make(chan string)
	quit_browser := make(chan bool)

	Conf := parser.ReadConfig("config.json")
	dev := usb.Device{VendorID: 0x1908, ProductID: 0x0225}
	Assets := usbRead(Label, Target, dev)
	socketName := browser.FindSocketName()
	mpvSocketPath := Conf.MpvPath
	uzblSocketPath := Conf.BrowserSocketDir + socketName
	splashFlag = false
	cmd := exec.Command("/usr/local/bin/uzbl-browser")
	execErr := cmd.Start()
	time.Sleep(1 * time.Second)
	if execErr != nil {
		log.Println(execErr.Error())
	}
	go browser.SocketWorker(uzblSocketPath, quit_browser)
	browser.HideStatusBar(uzblSocketPath)
	browser.Maximized(uzblSocketPath)
	//quit_browser <- true
	var state string
	state = "INIT"
	AssetCounter := 0
	// run infinite loop
	for {
		log.Println(state)

		switch state {
		case "INIT":
			state = initState(AssetCounter, Assets)
			break

		case "BROWSER":
			player.OffFullscreen(mpvSocketPath)
			go browser.SocketWorker(uzblSocketPath, quit_browser)
			state = browserState(AssetCounter, Assets, uzblSocketPath)
			if AssetCounter < len(Assets)-1 {
				AssetCounter = AssetCounter + 1
			} else {
				AssetCounter = 0
			}
			break

		case "VIDEO":
			state = videoState(AssetCounter, Assets, mpvSocketPath, playerState)
			if AssetCounter < len(Assets)-1 {
				AssetCounter = AssetCounter + 1
			} else {
				AssetCounter = 0
			}
			break

		case "SPLASH":
			state = splashState(AssetCounter, Assets, uzblSocketPath, Conf.IdlePagePath)
			time.Sleep(time.Second * 10)
			break
		}

	}

}
