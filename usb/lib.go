package usb

import (
	"log"
	"os"
	"os/exec"

	"github.com/google/gousb"
)

type Device struct {
	VendorID  gousb.ID
	ProductID gousb.ID
}

func InspectDevices() []Device {
	var deviceList []Device
	ctx := gousb.NewContext()
	ctx.OpenDevices(func(desc *gousb.DeviceDesc) bool {
		dev := Device{VendorID: desc.Vendor, ProductID: desc.Product}
		deviceList = append(deviceList, dev)
		return false
	})
	return deviceList
}

func CheckDeviceConnected(device Device) bool {
	devList := InspectDevices()
	for i := range devList {
		if devList[i].VendorID == device.VendorID && devList[i].ProductID == device.ProductID {
			return true
		}
	}
	return false
}

func MountDevice(label string, target string) bool {
	_, err := exec.Command("/bin/sh", "-c", "sudo mount -L "+label+" "+target).Output()
	//err := syscall.Mount(label, target, "auto", 3, "roL")
	if err != nil {
		log.Println(err.Error())
	}
	_, ExErr := os.Stat(target)
	if ExErr == nil {
		return true
	} else {
		return false
	}
}

func MonitorUSB(device Device, label string, target string, detectedPath chan string) {
	for {
		result := CheckDeviceConnected(device)
		if result == true {
			mountResult := MountDevice(label, target)
			if mountResult == true {
				detectedPath <- target
			}
		} else {
			detectedPath <- "NO_DEV"
		}
	}
}
