package browser

import (
	"log"
	"net"
	"os/exec"
	"regexp"
	"strings"
	"time"
)

var Buffer []string

func FindSocketName() string {
	out, err := exec.Command("bash", "-c", "ps aux | grep uzbl-core").Output()
	if err != nil {
		log.Println("Cannot execute ps command", err.Error())
	}
	stringVal := string(out[:])
	var re = regexp.MustCompile(`(?is)(^.*?\s+)(\d+)`)
	proc_num := strings.Fields(re.FindString(stringVal))[1]
	return "uzbl_socket_" + proc_num
}

func SocketConnect(socketPath string) (net.Conn, error) {
	sock, err := net.Dial("unix", socketPath)
	if err != nil {
		log.Fatal("Cannot dial to uzbl socketpath", err.Error())
		return sock, err
	}
	return sock, err
}

func SocketClose(socketHandler net.Conn) {
	err := socketHandler.Close()
	if err != nil {
		log.Fatal("Error during socket close", err.Error())
	}
}

func WriteCommand(command string) {
	SocketWriter(command)
}

func HideStatusBar(socketPath string) {
	WriteCommand("set show_status 0")
}

func ShowStatusBar(socketPath string) {
	WriteCommand("set show_status 1")
}

func Maximized(socketPath string) {
	WriteCommand("geometry maximized")
}

func FullScreenMode(socketPath string) {
	WriteCommand("set enable_fullscreen 1")
}

func LoadUrl(socketPath string, url string) {
	WriteCommand("uri " + url + "\n")
}

func DelayBrowser(output chan string, duration time.Duration) {
	time.Sleep(duration)
	output <- "DONE"
}

func SocketWorker(socketPath string, stop chan bool) {
	for {
		select {
		case <-stop:
			return
		default:
			if len(Buffer) > 0 {
				socketHandler, _ := SocketConnect(socketPath)
				defer SocketClose(socketHandler)
				_, err := socketHandler.Write([]byte(Buffer[0]))
				if err != nil {
					log.Println("Error during write to uzbl socket", err.Error())
					time.Sleep(1 * time.Second)
					break
				}
				Buffer = Buffer[1:]

			}
			time.Sleep(1 * time.Second)
		}

	}

}

func SocketWriter(commandParam string) {
	Buffer = append(Buffer, commandParam)
}
