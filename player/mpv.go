package player

import (
	"encoding/json"
	"log"
	"net"
	"strings"
	"time"
)

//Reply struct for replies from mpv socket
type Reply struct {
	Data  string `json:"data"`
	Error string `json:"error"`
}

//Event event reply struct
type Event struct {
	Name string `json:"event"`
}

var RecChannel chan string
var Buffer []string

//SocketConnect connect to socket file
func SocketConnect(socketPath string) (net.Conn, error) {
	sock, err := net.Dial("unix", socketPath)
	if err != nil {
		log.Fatal("Cannot dial to mpv socketpath", err.Error())
		return sock, err
	}
	return sock, err
}

func SocketClose(socketHandler net.Conn) {
	err := socketHandler.Close()
	if err != nil {
		log.Fatal("Error during socket close", err.Error())
	}
}

func WriteCommand(socketPath string, command string, params ...string) {
	socketHandler, _ := SocketConnect(socketPath)
	defer SocketClose(socketHandler)
	//string preparation
	var stringParams string
	var commandParam string
	for _, param := range params {
		stringParams = stringParams + `,"` + param + `"`
	}
	commandParam = `{"command":["` + command + `"` + stringParams + `]}` + "\n"
	SocketWriter(commandParam)
	/*status, err := socketHandler.Write([]byte(commandParam))
	if err != nil {
		log.Println("Error during write to mpv socket", err.Error())
	}
	return status*/
}

func SetBoolProperty(socketPath string, property string, value string) {
	socketHandler, _ := SocketConnect(socketPath)
	defer SocketClose(socketHandler)
	//string preparation
	commandParam := `{"command":["set_property","` + property + `",` + value + `]}` + "\n"
	SocketWriter(commandParam)
	/*status, err := socketHandler.Write([]byte(commandParam))
	if err != nil {
		log.Println("Error during write to mpv socket", err.Error())
	}*/
	//return status
}

func ReadSocket(socketPath string, output chan string) {
	socketHandler, _ := SocketConnect(socketPath)
	buf := make([]byte, 1024)
	for {
		n, err := socketHandler.Read(buf[:])
		if err != nil {
			return
		}
		output <- string(buf[0:n])

	}
}

func GetProperty(socketPath string, property string) {
	socketHandler, _ := SocketConnect(socketPath)
	defer SocketClose(socketHandler)
	commandParam := `{"command":["get_property","` + property + `"]}` + "\n"
	SocketWriter(commandParam)
	/*_, err := socketHandler.Write([]byte(commandParam))
	if err != nil {
		log.Println("Error during write to mpv socket", err.Error())
	}*/
}

func EndListener(output chan string) bool {
	var EndEvent Event
	for {
		msg := <-output
		//log.Println(msg)
		splittedMsg := strings.Split(msg, "\n")
		for i := 0; i < len(splittedMsg); i++ {
			_ = json.Unmarshal([]byte(splittedMsg[i]), &EndEvent)
			switch EndEvent.Name {
			case "end-file":
				return true
				/*case "idle":
				return true*/

			}
		}
	}

}

func LoadFile(socketPath string, path string) {
	WriteCommand(socketPath, "loadfile", path)
}

func OnFullscreen(socketPath string) {
	SetBoolProperty(socketPath, "fullscreen", "true")
}

func OffFullscreen(socketPath string) {
	SetBoolProperty(socketPath, "fullscreen", "false")
}

func SocketWorker(socketPath string, stop chan bool) {
	for {
		select {
		case <-stop:
			return
		default:
			if len(Buffer) > 0 {
				socketHandler, _ := SocketConnect(socketPath)
				defer SocketClose(socketHandler)
				_, err := socketHandler.Write([]byte(Buffer[0]))
				if err != nil {
					log.Println("Error during write to mpv socket", err.Error())
					time.Sleep(1 * time.Second)
					break
				}
				Buffer = Buffer[1:]

			}
			time.Sleep(1 * time.Second)
		}

	}

}

func SocketWriter(commandParam string) {
	Buffer = append(Buffer, commandParam)
}
