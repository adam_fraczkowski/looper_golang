package parser

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

type Config struct {
	MpvPath          string `json:"mpvPath"`
	BrowserSocketDir string `json:"browserSocketDir"`
	IdlePagePath     string `json:"IdlePagePath"`
	UsbPath          string `json:"usbPath"`
}

func ReadConfig(configPath string) Config {
	var confData Config

	config, err := os.Open(configPath)
	if err != nil {
		log.Fatal("Cannot read config", err.Error())
		return confData
	}
	byteData, _ := ioutil.ReadAll(config)
	errJSON := json.Unmarshal(byteData, &confData)
	if errJSON != nil {
		log.Fatal("Cannot read config", err.Error())
	}
	config.Close()
	return confData
}
