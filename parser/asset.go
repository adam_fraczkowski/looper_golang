package parser

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"path"
	"regexp"
	"strconv"
)

type Asset struct {
	AssetType     string `json:"type"`
	Path          string `json:"path"`
	Timeout       int64  `json:"timeout"`
	AssetID       string `json:"asset_id"`
	FromTimestamp int64  `json:"from_timestamp"`
	ToTimestamp   int64  `json:"to_timestamp"`
}

func ReadAssets(assetsPath string) []Asset {
	var assetData []Asset
	asset, err := os.Open(assetsPath)
	if err != nil {
		log.Fatal("Cannot read assets file", err.Error())
		return assetData
	}
	byteData, _ := ioutil.ReadAll(asset)
	errJSON := json.Unmarshal(byteData, &assetData)
	if errJSON != nil {
		log.Fatal("Cannot read asset", err.Error())
	}
	asset.Close()
	return assetData
}

func GetContentType(filePath string) (string, error) {
	f, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer f.Close()
	buffer := make([]byte, 512)
	_, errFile := f.Read(buffer)
	if errFile != nil {
		return "", err
	}
	contentType := http.DetectContentType(buffer)
	return contentType, nil
}

func GetAssetType(filePath string) string {
	fileTypes := map[string]string{
		"video/mp4":        "VIDEO",
		"video/mpeg":       "VIDEO",
		"video/quicktime":  "VIDEO",
		"video/webm":       "VIDEO",
		"video/x-flv":      "VIDEO",
		"video/x-matroska": "VIDEO",
		"video/x-ms-asf":   "VIDEO",
		"video/x-msvideo":  "VIDEO",
		"video/3gpp":       "VIDEO",
		"video/x-ms-wmv":   "VIDEO",
		"video/mp2t":       "VIDEO",
		"video/msvideo":    "VIDEO",
		"video/x-avi":      "VIDEO",
		"video/x-mpeg":     "VIDEO",
		"video/x-theora":   "VIDEO",
		"video/flv":        "VIDEO",
		"video/3gp":        "VIDEO",
		"video/divx":       "VIDEO",
		"video/avi":        "VIDEO",
		"text/plain":       "BROWSER",
		"text/html":        "BROWSER",
		"image/gif":        "BROWSER",
		"image/png":        "BROWSER",
		"image/svg+xml":    "BROWSER"}
	content, _ := GetContentType(filePath)
	return fileTypes[content]
}

func ReadFilesFromPath(FolderPath string) []Asset {
	var Assets []Asset
	files, err := ioutil.ReadDir(FolderPath)
	timeoutRegex := regexp.MustCompile(`(?i)^.*?#(\d+).*`)
	if err != nil {
		log.Println("Error during DIRLIST")
	}
	for _, f := range files {

		if f.IsDir() == false {
			AssetType := GetAssetType(path.Join(FolderPath, f.Name()))
			var FromTimestamp int64 = 0
			var ToTimestamp int64 = 0
			AssetID := f.Name() + "_" + strconv.Itoa(rand.Intn(1000))
			matchArray := timeoutRegex.FindAllString(f.Name(), -1)
			Path := path.Join(FolderPath, f.Name())
			var Timeout int64
			if len(matchArray) == 0 {
				Timeout = int64(30)
			} else {
				Timeout, err = strconv.ParseInt(matchArray[0], 10, 64)
				if err != nil {
					Timeout = int64(30)
				}
			}

			Assets = append(Assets, Asset{AssetType: AssetType,
				Path:          Path,
				Timeout:       Timeout,
				AssetID:       AssetID,
				FromTimestamp: FromTimestamp,
				ToTimestamp:   ToTimestamp})

		}

	}

	return Assets
}
